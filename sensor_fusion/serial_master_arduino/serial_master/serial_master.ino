#include "HX711.h"
#include <SoftwareSerial.h>

// HX711.DOUT	- pin #A1
// HX711.PD_SCK	- pin #A0
String str;
float val;
float bat;
float v_divider = (10+4.7)/4.7;

int analogPin = 2; // battery voltage
HX711 scale(A1, A0);		// parameter "gain" is ommited; the default value 128 is used by the library
SoftwareSerial gpsSerial(13, 12);
void setup() {
  Serial.begin(57600);		
  gpsSerial.begin(9600);	

  scale.set_scale(2280.f);                      // this value is obtained by calibrating the scale with known weights; see the README for details
//  scale.tare();				        // reset the scale to 0
  
}

void loop() {

  while (gpsSerial.available()){
    str = String(gpsSerial.readStringUntil('\n'));
    Serial.println(str);
  }
  val = (scale.get_units());
  Serial.println("loadWt," + String(val));
  
  bat = (analogRead(analogPin)*5)*v_divider/1024;    // read the input pin
  Serial.println("batVol," + String(bat));
  scale.power_down();			        // put the ADC in sleep mode
  delay(1000);
  scale.power_up();
}
