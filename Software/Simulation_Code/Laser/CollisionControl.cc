#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>

/*need to do this line in c++ only*/
using namespace PlayerCc;
using namespace std;

#include "CollisionControl.h"

/*Define Constants*/
//define crucial sonar ranges
const double k_Sens_Max_Dist = 5.0;
const double k_Sens_Mid_Dist = 2.0;
const double k_Sens_Min_Dist = 1.0;
const double k_Sens_Stop_Dist = 0.5;

//define laser counts by range
const double degree0 = 0;
const double degree45 = 90;
const double degree90 = 180;
const double degree135 = 270;
const double degree180 = 360;

//define num of sensors
const int numSensFront = 4;
const int numSensFrontL = 2;
const int numSensFrontR = 2;
const int numSensBack = 4;
const int numSensBackL = 2;
const int numSensBackR = 2;

/*Function used for deciphering the data read from the laser. Laser data is split into quadrants*/
double CollisionControl::DecipherLaserData(LaserProxy &laser, double startRange, double endRange)
{
   double objDistance = k_Sens_Max_Dist;
   double Dist = 0;

   for (int i = startRange; i <= endRange; i++)
   {
      Dist = laser.GetRange(i);
      if (Dist < objDistance)
         objDistance = Dist;
   }

   if (objDistance <= k_Sens_Max_Dist)
      return objDistance;
   else
      return k_Sens_Max_Dist;
}

/*Set bot speed and turn angle*/
void CollisionControl::SetBotControl(double wheelSpeed, double turnAngle, double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{
   *c_Wheel_Spd = wheelSpeed;
   *c_BotL_TurnSpeed = turnAngle;
}

/*Obstacle avoidance algorithm*/
void CollisionControl::ObstacleAvoid(LaserProxy &laser, double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{

   //define the front sensors
   m_SensC_FrontRR = DecipherLaserData(laser, degree0, degree45);
   m_SensC_FrontRC = DecipherLaserData(laser, degree45, degree90);
   m_SensC_FrontLC = DecipherLaserData(laser, degree90, degree135);
   m_SensC_FrontLL = DecipherLaserData(laser, degree135, degree180);

   printf("%f, %f, %f, %f\n", m_SensC_FrontRR, m_SensC_FrontRC, m_SensC_FrontLC, m_SensC_FrontLL);

   //define total distances per edge of robot
   frontTotL = m_SensC_FrontLL + m_SensC_FrontLC;
   frontTotR = m_SensC_FrontRR + m_SensC_FrontRC;
   frontTot = frontTotL + frontTotR;

   
   double tolerance = 0.5; //used when comparing left sensors vs right sensors

   //Only care if our sensors are sensing any objects within range
   //Otherwise leave speed and turn angle unchanged and allow robot to continue as is
   if (frontTot < numSensFront*k_Sens_Max_Dist && *c_Wheel_Spd != k_Wheel_Stop_Spd){
      //want to make a sharp turn on the spot if any of the sensors are within critical distance
      //A tolerance is used when comparing left sensors vs right sensors
      //This is necessary for rare cases where all sensors are within critical distance, in this case robot will reverse
      if (m_SensC_FrontRR <= k_Sens_Stop_Dist || m_SensC_FrontRC <= k_Sens_Stop_Dist || m_SensC_FrontLL <= k_Sens_Stop_Dist || m_SensC_FrontLC <= k_Sens_Stop_Dist){
         if (frontTotL + tolerance < frontTotR){
            if (m_SensC_FrontLL <= k_Sens_Stop_Dist && m_SensC_FrontLC <= k_Sens_Stop_Dist){
               SetBotControl(k_Wheel_Stop_Spd, turn180R, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
            else if (m_SensC_FrontLL <= k_Sens_Stop_Dist){
               SetBotControl(k_Wheel_Min_Spd, turn45R, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
            else if (m_SensC_FrontLC <= k_Sens_Stop_Dist){
               SetBotControl(k_Wheel_Stop_Spd, turn45R, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
         }
         else if (frontTotR + tolerance < frontTotL){
            if (m_SensC_FrontRR <= k_Sens_Stop_Dist && m_SensC_FrontRC <= k_Sens_Stop_Dist){
               SetBotControl(k_Wheel_Stop_Spd, turn180L, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
            else if (m_SensC_FrontRR <= k_Sens_Stop_Dist){
               SetBotControl(k_Wheel_Min_Spd, turn45L, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
            else if (m_SensC_FrontRC <= k_Sens_Stop_Dist){
               SetBotControl(k_Wheel_Stop_Spd, turn45L, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
         }
      }
      //None of the sensors are within critical distance, but within range of object
      //Must decrease speed and gradually adjust direction
      else if (!(m_SensC_FrontRR <= k_Sens_Stop_Dist || m_SensC_FrontRC <= k_Sens_Stop_Dist || m_SensC_FrontLL <= k_Sens_Stop_Dist || m_SensC_FrontLC <= k_Sens_Stop_Dist)){
         if (frontTotL <= frontTotR){
            if (m_SensC_FrontLL <= k_Sens_Min_Dist && m_SensC_FrontLC <= k_Sens_Min_Dist){
               SetBotControl(k_Wheel_Min_Spd, turn45R, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
            else if (m_SensC_FrontLL <= k_Sens_Min_Dist || m_SensC_FrontLC <= k_Sens_Min_Dist){
               SetBotControl(k_Wheel_Slow_Spd, turnSmallR, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
         }
         else if (frontTotL >= frontTotR){
            if (m_SensC_FrontRR <= k_Sens_Min_Dist && m_SensC_FrontRC <= k_Sens_Min_Dist){
               SetBotControl(k_Wheel_Min_Spd, turn45L, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
            else if (m_SensC_FrontRR <= k_Sens_Min_Dist || m_SensC_FrontRC <= k_Sens_Min_Dist){
               SetBotControl(k_Wheel_Slow_Spd, turnSmallL, c_Wheel_Spd, c_BotL_TurnSpeed);
            }
         }
         else{} //leave as set prior
      }
      else {} //leave as set prior
   }
   else{} //leave as set prior
}