#ifndef __COLLISIONCONTROL_H_INCLUDE__
#define __COLLISIONCONTROL_H_INCLUDE__

/*Define Constants*/
//define turn angles in radians
const double turn180R = -3.14159;
const double turn180L = 3.14159;
const double turn90R = -3.14159/2;
const double turn90L = 3.14159/2;
const double turn45R = -3.14159/4;
const double turn45L = 3.14159/4;
const double turnSmallR = -3.14159/6;
const double turnSmallL = 3.14159/6;
const double turnNone = 0.0;

//define robot control speeds
//For real robot, this will act as percentage of the voltage applied to each motor
const double k_Wheel_Max_Spd = 1;
const double k_Wheel_Slow_Spd = 0.3;
const double k_Wheel_Min_Spd = 0.1;
const double k_Wheel_Stop_Spd = 0.0;

//external constants




class CollisionControl {
   double m_SensC_FrontRR, m_SensC_FrontRC, m_SensC_FrontLC, m_SensC_FrontLL, frontTotR, frontTotL, frontTot;
   double DecipherLaserData(LaserProxy &laser, double startRange, double endRange);
   void SetBotControl(double wheelSpeed, double turnAngle, double *c_Wheel_Spd, double *c_BotL_TurnSpeed);
public:
	void ObstacleAvoid(LaserProxy &laser, double *c_Wheel_Spd, double *c_BotL_TurnSpeed);
};

#endif // __COLLISIONCONTROL_H_INCLUDE__