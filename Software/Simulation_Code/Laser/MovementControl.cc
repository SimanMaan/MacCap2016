#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>
#include <math.h>

/*need to do this line in c++ only*/
using namespace PlayerCc;
using namespace std;

#include "MovementControl.h"

/*Function for determining the optimal path to desired location*/
void MovementControl::ShortestPath(Position2dProxy &pp, double *c_Wheel_Spd, double *c_BotL_TurnSpeed, string FileName,
   double TargetXPos, double TargetYPos, int *array_incrementor, int *array_position, bool *followBallPathing)
{

   //Obtain the current orientation of robot relative to world
   m_BotL_XPos = pp.GetXPos();  //Position according to grid (not relative to start as per documentation)
   m_BotL_YPos = pp.GetYPos();  //Position according to grid (not relative to start as per documentation)
   m_BotL_Yaw = pp.GetYaw();    //Angles returned between -180 -> 180 degrees

   //temporary value for turn
   double turnSP = 0;

   //Define the target X and Y coordinates
   c_BotL_Next_XPos = TargetXPos;
   c_BotL_Next_YPos = TargetYPos;

   //These distances are used to ensure robot slows down during approach
   //Also ensure robot stops within specified distance of target destination

   double distTol = 0.2; //tolerance distance within desired position
   double distSlow = 1.0; //distance in which robot must slow down

   //Calculate optimal path (triangle)
   double DistX = c_BotL_Next_XPos - m_BotL_XPos;
   double DistY = c_BotL_Next_YPos - m_BotL_YPos;
   double DistHyp = sqrt(pow(DistX,2) + pow(DistY,2));


   //m_BotL_Yaw is between -180 -> 180 degrees
   //Calculate the appropriate turn angle
   //Only need to worry about when DistX negative
   if (DistX < 0 && m_BotL_Yaw > 0)
      turnSP = (dtor(angle180) + atan(DistY/DistX)) - m_BotL_Yaw;
   else if (DistX < 0 && m_BotL_Yaw < 0)
      turnSP = atan(DistY/DistX) - (dtor(angle180) + m_BotL_Yaw);
   else
      turnSP = atan(DistY/DistX) - m_BotL_Yaw;

   //If it is faster to turn the other way then change to that angle
   if (abs(turnSP) > (dtor(angle360) - abs(turnSP))){
      if (turnSP > 0)
         turnSP = dtor(angle360) - turnSP;
      else
         turnSP = dtor(angle360) + turnSP;
   }

   //set the speed and turn angle for the robot
   *c_BotL_TurnSpeed = turnSP;
   *c_Wheel_Spd = k_Wheel_Max_Spd;


   //if robot in approach phase (within 1m DistHyp), slow down relative to distance
   //if the robot is within destination plus tolerance then stop 
   if (DistHyp > distTol && DistHyp <= distSlow)
      *c_Wheel_Spd = (k_Wheel_Max_Spd * DistHyp)/2;
   else if (DistHyp <= distTol && DistHyp  >= -1*distTol){
      *c_Wheel_Spd = k_Wheel_Stop_Spd;
      *c_BotL_TurnSpeed = turnNone;
      if (*array_incrementor <= *array_position - 2)
         *array_incrementor = *array_incrementor + 2;

      //if (FileName == pathing_coordinates)
         //arrayPath_incrementor = array_incrementor;
      if (FileName == ball_coordinates) {
         //arrayBall_incrementor = 0;
         *array_incrementor = 0;
         *followBallPathing = false;
      }
   }
      
      printf("%f, %f\n", c_BotL_Next_XPos, c_BotL_Next_YPos);
}

void MovementControl::RandomPath(double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{

   double randAngle;

   *c_Wheel_Spd = k_Wheel_Max_Spd;

   //max turn angles -90 degrees to 90 degrees
   randAngle = rand()% (int(angle180) + 1); //generates random value within 0-180
   randAngle = randAngle - angle180/2; //keeps turning angle between -90 and 90 degrees

   *c_BotL_TurnSpeed = dtor(randAngle) ; //converts from degrees to radians
}

/*Allow robot to move in straight path*/
void MovementControl::MoveStraight(double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{
   *c_Wheel_Spd = k_Wheel_Max_Spd;
   *c_BotL_TurnSpeed = turnNone;
}