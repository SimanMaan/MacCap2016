#ifndef __MOVEMENTCONTROL_H_INCLUDE
#define __MOVEMENTCONTROL_H_INCLUDE

#include "CollisionControl.h"

/*Declerations of Constants*/
//define neccessary files
extern const double turnNone;
//const double k_Wheel_Max_Spd = 1;
extern const double k_Wheel_Stop_Spd;
//const double k_Wheel_Stop_Spd = 0.0;

//define angles
const double angle180 = 180.0;
const double angle360 = 360.0;

//define file names
const string pathing_coordinates = "pathing_coordinates.txt";
const string ball_coordinates = "ball_coordinates.txt";

class MovementControl{
	double m_BotL_XPos, m_BotL_YPos, m_BotL_Yaw, c_BotL_Next_XPos, c_BotL_Next_YPos;
public:
	void ShortestPath(Position2dProxy &pp, double *c_Wheel_Spd, double *c_BotL_TurnSpeed, string FileName,
	   double TargetXPos, double TargetYPos, int *array_incrementor, int *array_position, bool *followBallPathing);
	void RandomPath(double *c_Wheel_Spd, double *c_BotL_TurnSpeed);
	void MoveStraight(double *c_Wheel_Spd, double *c_BotL_TurnSpeed);
};

#endif // __PATHCONTROL_H_INCLUDE
