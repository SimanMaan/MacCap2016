#ifndef __PATHCONTROL_H_INCLUDE
#define __PATHCONTROL_H_INCLUDE

/*Includes*/
#include "MovementControl.h"
#include "CollisionControl.h"

//define file names
extern const string pathing_coordinates;
extern const string ball_coordinates;
extern const double turnNone;
extern const double k_Wheel_Stop_Spd;


class PathControl{
	MovementControl robotm;
	CollisionControl robotc;
	bool FileReadtoArray(string FileName, double array_coordinates[], int *array_pos);
public:
	void PathInstructions(Position2dProxy &posp, LaserProxy &laserp, double *c_Wheel_Spd, double *c_BotL_TurnSpeed);
};

#endif // __PATHCONTROL_H_INCLUDE