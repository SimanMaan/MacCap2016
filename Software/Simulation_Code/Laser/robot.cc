#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>

/*need to do this line in c++ only*/
using namespace PlayerCc;
using namespace std;

#include "PathControl.h"

//By default, accept random pathing. If exists pathing instructions, follow those instead, stop when complete.
//If exists override instructions, follow those, once completed continue as before
int main(int argc, char *argv[])
{
   //Establish connection with stage robot 6665
   //Did not need to specify 6665 as it is default for 1 robot
   //Establish connection with Position proxy and Sonar proxy
	PlayerClient robot("localhost");
	Position2dProxy p2dProxy(&robot,0);
   LaserProxy laserProxy(&robot,0);

	//enable motors
	p2dProxy.SetMotorEnable(1);

	//variable declerations for modifying robots speed and direction
	double c_Wheel_Spd, c_BotL_TurnSpeed;

   //object decleration for path control
   PathControl robotp;

	while(true)
	{
      //Request Geometries
      //rangerProxy.RequestGeom();
      p2dProxy.RequestGeom();
      //Use line below for Stage 2D simulation. Remove for Gazebo 3D
      laserProxy.Configure(dtor(0), dtor(180), 100, 1, false, 10);
      //laserProxy.RequestGeom(); Dont think this is necessary. Causes segmentation fault?
         
		// read from the proxies
		robot.Read();

      robotp.PathInstructions(p2dProxy, laserProxy, &c_Wheel_Spd, &c_BotL_TurnSpeed);
      
		//set motors
		p2dProxy.SetSpeed(c_Wheel_Spd, c_BotL_TurnSpeed);
	}
   
	return 0;
}
