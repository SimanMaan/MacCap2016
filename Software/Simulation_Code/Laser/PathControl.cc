#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <string>

/*need to do this line in c++ only*/
using namespace PlayerCc;
using namespace std;

#include "PathControl.h"

//Define variables and arrays used for pathing
bool  followPathing = false;
double array_path[100];
int arrayPath_incrementor = 0;
int arrayPath_position = 0; //this will be used incremently to fill array

//define variables and arrays used for ball pathing
bool followBallPathing = false;
double array_ball[10];
int arrayBall_incrementor = 0;
int arrayBall_position = 0;

/*Read pathing coordinates from external file*/
bool PathControl::FileReadtoArray(string FileName, double array_coordinates[], int *array_pos)
{
   //int array_pos = 0;
   *array_pos = 0;

   //Initialize variables
   fstream cordFile;
   string fileData;

   //Using ifstream, open file and check if open succeeded
   //While data exists, read data into string and parse by ','
   //Pass each element into array
   //This may change later based on most convenient method
   cordFile.open(FileName, ios::in | ios::out);

   if (cordFile.is_open()){
      if (cordFile.peek() == std::ifstream::traits_type::eof()){
         return false;
      }
      while (getline(cordFile, fileData)){
         string splitData;
         istringstream ss(fileData);
         while(getline(ss, splitData, ',')){
            array_coordinates[*array_pos] = stod(splitData);
            *array_pos = *array_pos + 1;
         }
      }

      if (FileName == ball_coordinates){
         cordFile.close();
         cordFile.open(FileName, ios::out |ios::trunc);
         cordFile.close();
      }
      else{
         cordFile.close();
      }

      return true;
   }

   return false;
}

void PathControl::PathInstructions(Position2dProxy &posp, LaserProxy &laserp, double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{
	//Check if there is a pathing algorithm to follow
	if (!followPathing)
		followPathing = FileReadtoArray(pathing_coordinates, array_path, &arrayPath_position);

	//check if theres ball pathing instructions
	if (!followBallPathing)
		followBallPathing = FileReadtoArray(ball_coordinates, array_ball, &arrayBall_position);

	//Move Straight
	//robot.MoveStraight(&c_Wheel_Spd, &c_BotL_TurnSpeed);
  	robotm.RandomPath(c_Wheel_Spd, c_BotL_TurnSpeed);

  	//Move to Objective - Shortest Path
  	if (followBallPathing && arrayBall_incrementor <= arrayBall_position -2)
     	robotm.ShortestPath(posp, c_Wheel_Spd, c_BotL_TurnSpeed, ball_coordinates, array_ball[arrayBall_incrementor], 
     		array_ball[arrayBall_incrementor + 1], &arrayBall_incrementor, &arrayBall_position, &followBallPathing);
  	else if (followPathing){
     	if (arrayPath_incrementor <= arrayPath_position - 2)
        	robotm.ShortestPath(posp, c_Wheel_Spd, c_BotL_TurnSpeed, pathing_coordinates, array_path[arrayPath_incrementor], 
        		array_path[arrayPath_incrementor + 1], &arrayPath_incrementor, &arrayPath_position, &followBallPathing);
     	else{
        	*c_Wheel_Spd = k_Wheel_Stop_Spd;
        	*c_BotL_TurnSpeed = turnNone;
     	}
  	}

	//Avoid Obstacle
  //This function must be called last as it is the most crucial, will modify speed and turn if necessary
	robotc.ObstacleAvoid(laserp, c_Wheel_Spd, c_BotL_TurnSpeed);
}