#include <stdio.h>
#include <stdlib.h>
#include <libplayerc++/playerc++.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <istream>
#include <string>


/*need to do this line in c++ only*/
using namespace PlayerCc;
using namespace std;

//define neccessary files
string pathing_coordinates = "pathing_coordinates.txt";
string ball_coordinates = "ball_coordinates.txt";

//define num of sensors
int numSensFront = 4;
int numSensFrontL = 2;
int numSensFrontR = 2;
int numSensBack = 4;
int numSensBackL = 2;
int numSensBackR = 2;

//define turn angles in radians
double turn180R = -3.14159*2;
double turn180L = 3.14159*2;
double turn90R = -3.14159;
double turn90L = 3.14159;
double turn45R = -3.14159/2;
double turn45L = 3.14159/2;
double turnSmallR = -3.14159/5;
double turnSmallL = 3.14159/5;
double turnNone = 0.0;


double angle180 = 180.0;
double angle360 = 360.0;


//define crucial sonar ranges
double k_Sens_Max_Dist = 5.0;
double k_Sens_Mid_Dist = 2.0;
double k_Sens_Min_Dist = 1.0;
double k_Sens_Stop_Dist = 0.5;

//define robot control speeds
//For real robot, this will act as percentage of the voltage applied to each motor
double k_Wheel_Max_Spd = 1;
double k_Wheel_Slow_Spd = 0.3;
double k_Wheel_Min_Spd = 0.1;
double k_Wheel_Stop_Spd = 0.0;
double k_Wheel_Rev_Spd = -0.1;

//Define variables and arrays used for pathing
bool  followPathing = false;
double array_path[100];
int arrayPath_incrementor = 0;
int arrayPath_position = 0; //this will be used incremently to fill array

//define variables and arrays used for ball pathing
bool followBallPathing = false;
double array_ball[10];
int arrayBall_incrementor = 0;
int arrayBall_position = 0;


/*Obstacle avoidance algorithm*/
void ObstacleAvoid(SonarProxy &ranger, double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{
   //ranger.RequestConfigure();

   //define the front sensors
   double m_SensC_FrontRR = ranger[6];
   double m_SensC_FrontRC = ranger[4];
   double m_SensC_FrontLC = ranger[3];
   double m_SensC_FrontLL = ranger[1];

   //define the back sensors
   double m_SensC_BackRR = ranger[9];
   double m_SensC_BackRC = ranger[11];
   double m_SensC_BackLC = ranger[12];
   double m_SensC_BackLL = ranger[14];


   //define total distances per edge of robot
   double frontTotL = m_SensC_FrontLL + m_SensC_FrontLC;
   double frontTotR = m_SensC_FrontRR + m_SensC_FrontRC;
   double frontTot = frontTotL + frontTotR;

   double backTotL = m_SensC_BackLL + m_SensC_BackLC;
   double backTotR = m_SensC_BackRR + m_SensC_BackRC;
   double backTot = backTotL + backTotR;
  	
   double tolerance = 0.5; //used when comparing left sensors vs right sensors

   //Only care if our sensors are sensing any objects within range
   //Otherwise leave speed and turn angle unchanged and allow robot to continue as is
   if (frontTot < numSensFront*k_Sens_Max_Dist && *c_Wheel_Spd != k_Wheel_Stop_Spd)
   {
      //want to make a sharp turn on the spot if any of the sensors are within critical distance
      //A tolerance is used when comparing left sensors vs right sensors
      //This is necessary for rare cases where all sensors are within critical distance, in this case robot will reverse
      if (m_SensC_FrontRR <= k_Sens_Stop_Dist || m_SensC_FrontRC <= k_Sens_Stop_Dist || m_SensC_FrontLL <= k_Sens_Stop_Dist || m_SensC_FrontLC <= k_Sens_Stop_Dist){
      	if (frontTotL + tolerance < frontTotR){
            if (m_SensC_FrontLL <= k_Sens_Stop_Dist && m_SensC_FrontLC <= k_Sens_Stop_Dist){
               *c_Wheel_Spd = k_Wheel_Stop_Spd;
      			*c_BotL_TurnSpeed = turn180R;
            }
            else if (m_SensC_FrontLL <= k_Sens_Stop_Dist){
               *c_Wheel_Spd = k_Wheel_Min_Spd;
               *c_BotL_TurnSpeed = turn45R;
            }
            else if (m_SensC_FrontLC <= k_Sens_Stop_Dist){
               *c_Wheel_Spd = k_Wheel_Stop_Spd;
               *c_BotL_TurnSpeed = turn45R;
            }
         }
         else if (frontTotR + tolerance < frontTotL){
            if (m_SensC_FrontRR <= k_Sens_Stop_Dist && m_SensC_FrontRC <= k_Sens_Stop_Dist){
               *c_Wheel_Spd = k_Wheel_Stop_Spd;
               *c_BotL_TurnSpeed = turn180L;
            }
            else if (m_SensC_FrontRR <= k_Sens_Stop_Dist){
               *c_Wheel_Spd = k_Wheel_Min_Spd;
               *c_BotL_TurnSpeed = turn45L;
            }
            else if (m_SensC_FrontRC <= k_Sens_Stop_Dist){
               *c_Wheel_Spd = k_Wheel_Stop_Spd;
               *c_BotL_TurnSpeed = turn45L;
            }
         }
         else{
            if (!(m_SensC_BackRR <= k_Sens_Stop_Dist || m_SensC_BackRC <= k_Sens_Stop_Dist || m_SensC_BackLL <= k_Sens_Stop_Dist || m_SensC_BackLC <= k_Sens_Stop_Dist)){
               *c_Wheel_Spd = k_Wheel_Rev_Spd;
               *c_BotL_TurnSpeed = turnNone;
            }
         }
      }
      //None of the sensors are within critical distance, but within range of object
      //Must decrease speed and gradually adjust direction
      else if (!(m_SensC_FrontRR <= k_Sens_Stop_Dist || m_SensC_FrontRC <= k_Sens_Stop_Dist || m_SensC_FrontLL <= k_Sens_Stop_Dist || m_SensC_FrontLC <= k_Sens_Stop_Dist)){
         if (frontTotL <= frontTotR){
            if (m_SensC_FrontLL <= k_Sens_Min_Dist && m_SensC_FrontLC <= k_Sens_Min_Dist){
               *c_Wheel_Spd = k_Wheel_Min_Spd;
               *c_BotL_TurnSpeed = turn45R;
            }
            else if (m_SensC_FrontLL <= k_Sens_Min_Dist || m_SensC_FrontLC <= k_Sens_Min_Dist){
               *c_Wheel_Spd = k_Wheel_Slow_Spd;
               *c_BotL_TurnSpeed = turnSmallR;
            }
         }
         else if (frontTotL >= frontTotR){
            if (m_SensC_FrontRR <= k_Sens_Min_Dist && m_SensC_FrontRC <= k_Sens_Min_Dist){
               *c_Wheel_Spd = k_Wheel_Min_Spd;
               *c_BotL_TurnSpeed = turn45L;
            }
            else if (m_SensC_FrontRR <= k_Sens_Min_Dist || m_SensC_FrontRC <= k_Sens_Min_Dist){
               *c_Wheel_Spd = k_Wheel_Slow_Spd;
               *c_BotL_TurnSpeed = turnSmallL;
            }
         }
         else{} //leave as set prior
      }
      else {} //leave as set prior
   }
   else{} //leave as set prior
}



/*Function for determining the optimal path to desired location*/
void ShortestPath(Position2dProxy &pp, double *c_Wheel_Spd, double *c_BotL_TurnSpeed, string FileName,
   double TargetXPos, double TargetYPos, int *array_incrementor, int *array_position)
{

   //Obtain the current orientation of robot relative to world
   double m_BotL_XPos = pp.GetXPos();  //Position according to grid (not relative to start as per documentation)
   double m_BotL_YPos = pp.GetYPos();  //Position according to grid (not relative to start as per documentation)
   double m_BotL_Yaw = pp.GetYaw();    //Angles returned between -180 -> 180 degrees

   //temporary value for turn
   double turnSP = 0;

   //Define the target X and Y coordinates
   double c_BotL_Next_XPos = TargetXPos;
   double c_BotL_Next_YPos = TargetYPos;

   //These distances are used to ensure robot slows down during approach
   //Also ensure robot stops within specified distance of target destination

   double distTol = 0.2; //tolerance distance within desired position
   double distSlow = 1.0; //distance in which robot must slow down

   //Calculate optimal path (triangle)
   double DistX = c_BotL_Next_XPos - m_BotL_XPos;
   double DistY = c_BotL_Next_YPos - m_BotL_YPos;
   double DistHyp = sqrt(pow(DistX,2) + pow(DistY,2));


   //m_BotL_Yaw is between -180 -> 180 degrees
   //Calculate the appropriate turn angle
   //Only need to worry about when DistX negative
   if (DistX < 0 && m_BotL_Yaw > 0)
      turnSP = (dtor(angle180) + atan(DistY/DistX)) - m_BotL_Yaw;
   else if (DistX < 0 && m_BotL_Yaw < 0)
      turnSP = atan(DistY/DistX) - (dtor(angle180) + m_BotL_Yaw);
   else
      turnSP = atan(DistY/DistX) - m_BotL_Yaw;

   //If it is faster to turn the other way then change to that angle
   if (abs(turnSP) > (dtor(angle360) - abs(turnSP))){
      if (turnSP > 0)
         turnSP = dtor(angle360) - turnSP;
      else
         turnSP = dtor(angle360) + turnSP;
   }

   //set the speed and turn angle for the robot
   *c_BotL_TurnSpeed = turnSP;
   *c_Wheel_Spd = k_Wheel_Max_Spd;


   //if robot in approach phase (within 1m DistHyp), slow down relative to distance
   //if the robot is within destination plus tolerance then stop 
   if (DistHyp > distTol && DistHyp <= distSlow)
      *c_Wheel_Spd = (k_Wheel_Max_Spd * DistHyp)/2;
   else if (DistHyp <= distTol && DistHyp  >= -1*distTol){
      *c_Wheel_Spd = k_Wheel_Stop_Spd;
      *c_BotL_TurnSpeed = turnNone;
      if (*array_incrementor <= *array_position - 2)
         *array_incrementor = *array_incrementor + 2;

      //if (FileName == pathing_coordinates)
         //arrayPath_incrementor = array_incrementor;
      if ( FileName == ball_coordinates) {
         //arrayBall_incrementor = 0;
         *array_incrementor = 0;
         followBallPathing = false;
      }
   }
      
      printf("%f, %f\n", c_BotL_Next_XPos, c_BotL_Next_YPos);
}


void RandomPath(double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{

   double randAngle;

   *c_Wheel_Spd = k_Wheel_Max_Spd;

   //max turn angles -90 degrees to 90 degrees
   randAngle = rand()% (int(angle180) + 1); //generates random value within 0-180
   randAngle = randAngle - angle180/2; //keeps turning angle between -90 and 90 degrees

   *c_BotL_TurnSpeed = dtor(randAngle) ; //converts from degrees to radians
}


/*Allow robot to move in straight path*/
void MoveStraight(double *c_Wheel_Spd, double *c_BotL_TurnSpeed)
{
	*c_Wheel_Spd = k_Wheel_Max_Spd;
	*c_BotL_TurnSpeed = turnNone;
}


/*Read pathing coordinates from external file*/
bool FileReadtoArray(string FileName, double array_coordinates[], int *array_pos)
{
   //int array_pos = 0;
   *array_pos = 0;

   //Initialize variables
   fstream cordFile;
   string fileData;

   //Using ifstream, open file and check if open succeeded
   //While data exists, read data into string and parse by ','
   //Pass each element into array
   //This may change later based on most convenient method
   cordFile.open(FileName, ios::in | ios::out);

   if (cordFile.is_open()){
      if (cordFile.peek() == std::ifstream::traits_type::eof()){
         return false;
      }
      while (getline(cordFile, fileData)){
         string splitData;
         istringstream ss(fileData);
         while(getline(ss, splitData, ',')){
            array_coordinates[*array_pos] = stod(splitData);
            *array_pos = *array_pos + 1;
         }
      }

      if (FileName == ball_coordinates){
         cordFile.close();
         cordFile.open(FileName, ios::out |ios::trunc);
         cordFile.close();

         //arrayBall_position = array_pos;
      }
      else{
         //arrayPath_position = array_pos;
         cordFile.close();
      }

      return true;
   }

   return false;
}



//By default, accept random pathing. If exists pathing instructions, follow those instead, stop when complete.
//If exists override instructions, follow those, once completed continue as before
int main(int argc, char *argv[])
{
   //Establish connection with stage robot 6665
   //Did not need to specify 6665 as it is default for 1 robot
   //Establish connection with Position proxy and Sonar proxy
	PlayerClient robot("localhost");
	Position2dProxy p2dProxy(&robot,0);
	SonarProxy rangerProxy(&robot,0);

	//enable motors
	p2dProxy.SetMotorEnable(1);

	//variable declerations for modifying robots speed and direction
	double c_Wheel_Spd, c_BotL_TurnSpeed;

   //Check if there is a pathing algorithm to follow
   followPathing = FileReadtoArray(pathing_coordinates, array_path, &arrayPath_position);

	while(true)
	{
      //Request Geometries
      p2dProxy.RequestGeom();
      rangerProxy.RequestGeom();

		// read from the proxies
		robot.Read();

      //check if theres ball pathing instructions
      if (!followBallPathing)
         followBallPathing = FileReadtoArray(ball_coordinates, array_ball, &arrayBall_position);

		//Move Straight
		//MoveStraight(&c_Wheel_Spd, &c_BotL_TurnSpeed);
      RandomPath(&c_Wheel_Spd, &c_BotL_TurnSpeed);

      //Move to Objective - Shortest Path
      if (followBallPathing && arrayBall_incrementor <= arrayBall_position -2)
         ShortestPath(p2dProxy, &c_Wheel_Spd, &c_BotL_TurnSpeed, ball_coordinates,
            array_ball[arrayBall_incrementor], array_ball[arrayBall_incrementor + 1], &arrayBall_incrementor, &arrayBall_position);
      else if (followPathing){
         if (arrayPath_incrementor <= arrayPath_position - 2)
            ShortestPath(p2dProxy, &c_Wheel_Spd, &c_BotL_TurnSpeed, pathing_coordinates, 
               array_path[arrayPath_incrementor], array_path[arrayPath_incrementor + 1], &arrayPath_incrementor, &arrayPath_position);
         else{
            c_Wheel_Spd = k_Wheel_Stop_Spd;
            c_BotL_TurnSpeed = turnNone;
         }
      }

		//Avoid Obstacle
      //This function must be called last as it is the most crucial, will modify speed and turn if necessary
		ObstacleAvoid(rangerProxy, &c_Wheel_Spd, &c_BotL_TurnSpeed);

		//set motors
		p2dProxy.SetSpeed(c_Wheel_Spd, c_BotL_TurnSpeed);

	}
   
	return 0;
}
