#include <stdio.h>
#include <libplayerc++/playerc++.h>
#include <math.h>


/*need to do this line in c++ only*/
using namespace PlayerCc;

//define num of sensors
int numSensFront = 4;
int numSensFrontL = 2;
int numSensFrontR = 2;
int numSensBack = 4;
int numSensBackL = 2;
int numSensBackR = 2;

//define turn angles in radians
double turn180R = -3.14159*2;
double turn180L = 3.14159*2;
double turn90R = -3.14159;
double turn90L = 3.14159;
double turn45R = -3.14159/2;
double turn45L = 3.14159/2;
double turnSmallR = -3.14159/5;
double turnSmallL = 3.14159/5;
double turnNone = 0;

//define crucial sonar ranges
double distMax = 5.0;
double distLong = 2.0;
double distShort = 1.0;
double distStop = 0.5;

//define robot control speeds
//For real robot, this will act as percentage of the voltage applied to each motor
double speedMax = 1;
double speedSlow = 0.3;
double speedMin = 0.1;
double speedStop = 0;
double speedRev = -0.1;


/*Obstacle avoidance algorithm*/
void ObstacleAvoid(SonarProxy &ranger, double *forwardSpeed, double *turnSpeed)
{
   	//ranger.RequestConfigure();

      //define the front sensors
   	double rangeFrontRR = ranger[6];
   	double rangeFrontRC = ranger[4];
   	double rangeFrontLC = ranger[3];
   	double rangeFrontLL = ranger[1];

   	//define the back sensors
   	double rangeBackRR = ranger[9];
   	double rangeBackRC = ranger[11];
      double rangeBackLC = ranger[12];
   	double rangeBackLL = ranger[14];


   	//define total distances per edge of robot
      double frontTotL = rangeFrontLL + rangeFrontLC;
   	double frontTotR = rangeFrontRR + rangeFrontRC;
   	double frontTot = frontTotL + frontTotR;

   	double backTotL = rangeBackLL + rangeBackLC;
   	double backTotR = rangeBackRR + rangeBackRC;
   	double backTot = backTotL + backTotR;
  	
      double tolerance = 0.5; //used when comparing left sensors vs right sensors

      //Only care if our sensors are sensing any objects within range
      //Otherwise leave speed and turn angle unchanged and allow robot to continue as is
      if (frontTot < numSensFront*distMax)
      {
         //want to make a sharp turn on the spot if any of the sensors are within critical distance
         //A tolerance is used when comparing left sensors vs right sensors
         //This is necessary for rare cases where all sensors are within critical distance, in this case robot will reverse
      	if (rangeFrontRR <= distStop || rangeFrontRC <= distStop || rangeFrontLL <= distStop || rangeFrontLC <= distStop){
      		if (frontTotL + tolerance < frontTotR){
               if (rangeFrontLL <= distStop && rangeFrontLC <= distStop){
                  *forwardSpeed = speedStop;
      			   *turnSpeed = turn180R;
               }
               else if (rangeFrontLL <= distStop){
                  *forwardSpeed = speedMin;
                  *turnSpeed = turn45R;
               }
               else if (rangeFrontLC <= distStop){
                  *forwardSpeed = speedStop;
                  *turnSpeed = turn45R;
               }
            }
            else if (frontTotR + tolerance < frontTotL){
               if (rangeFrontRR <= distStop && rangeFrontRC <= distStop){
                  *forwardSpeed = speedStop;
                  *turnSpeed = turn180L;
               }
               else if (rangeFrontRR <= distStop){
                  *forwardSpeed = speedMin;
                  *turnSpeed = turn45L;
               }
               else if (rangeFrontRC <= distStop){
                  *forwardSpeed = speedStop;
                  *turnSpeed = turn45L;
               }
            }
            else{
               if (!(rangeBackRR <= distStop || rangeBackRC <= distStop || rangeBackLL <= distStop || rangeBackLC <= distStop)){
                  *forwardSpeed = speedRev;
                  *turnSpeed = turnNone;
               }
            }
      	}
         //None of the sensors are within critical distance, but within range of object
         //Must decrease speed and gradually adjust direction
         else if (!(rangeFrontRR <= distStop || rangeFrontRC <= distStop || rangeFrontLL <= distStop || rangeFrontLC <= distStop)){
            if (frontTotL <= frontTotR){
               if (rangeFrontLL <= distShort && rangeFrontLC <= distShort){
                  *forwardSpeed = speedMin;
                  *turnSpeed = turn45R;
               }
               else if (rangeFrontLL <= distShort || rangeFrontLC <= distShort){
                  *forwardSpeed = speedSlow;
                  *turnSpeed = turnSmallR;
               }
            }
            else if (frontTotL >= frontTotR){
               if (rangeFrontRR <= distShort && rangeFrontRC <= distShort){
                  *forwardSpeed = speedMin;
                  *turnSpeed = turn45L;
               }
               else if (rangeFrontRR <= distShort || rangeFrontRC <= distShort){
                  *forwardSpeed = speedSlow;
                  *turnSpeed = turnSmallL;
               }
            }
            else{} //leave as set prior
         }
         else {} //leave as set prior
      }
      else{} //leave as set prior
}


void ShortestPath(Position2dProxy &pp, double *forwardSpeed, double *turnSpeed)
{

   //Obtain the current orientation of robot relative to world
   double CurrentXPos = pp.GetXPos();
   double CurrentYPos = pp.GetYPos();
   double CurrentYaw = pp.GetYaw();

   //temporary value for turn
   double turnSP = 0;

   //Currently hard coded for testing
   double DesiredXPos = 5;
   double DesiredYPos = -3;

   //These distances are used to ensure robot slows down during approach
   //Also ensure robot stops within specified distance of target destination

   double distTol = 0.2; //tolerance distance within desired position
   double distSlow = 1.0; //distance in which robot must slow down

   //Calculate optimal path (triangle)
   double DistX = DesiredXPos - CurrentXPos;
   double DistY = DesiredYPos - CurrentYPos;
   double DistHyp = sqrt(pow(DistX,2) + pow(DistY,2));


   //Calculate optimal turn angle
   if (CurrentXPos > DesiredXPos && CurrentYPos > DesiredYPos)
      turnSP = -1*(atan(DistY/DistX) + dtor(90)) - CurrentYaw;
   else if (CurrentXPos < DesiredXPos && CurrentYPos > DesiredYPos)
      turnSP = atan(DistY/DistX) - CurrentYaw;
   else if (CurrentXPos > DesiredXPos && CurrentYPos < DesiredYPos)
      turnSP = -1*(atan(DistY/DistX) - dtor(90)) - CurrentYaw;
   else
      turnSP = atan(DistY/DistX) - CurrentYaw;


   //set the speed and turn angle for the robot
   *turnSpeed = turnSP;
   *forwardSpeed = speedMax;

   //if robot in approach phase (within 1m DistHyp), slow down relative to distance
   //if the robot is within destination plus tolerance then stop 
   if (DistHyp > distTol && DistHyp <= distSlow )
      *forwardSpeed = speedMax * DistHyp;
   else if (DistHyp <= distTol && DistHyp  >= -1*distTol){
      *forwardSpeed = speedStop;
      *turnSpeed = turnNone;
   }
}


void MoveStraight(double *forwardSpeed, double *turnSpeed)
{
	*forwardSpeed = speedMax;
	*turnSpeed = turnNone;
}



int main(int argc, char *argv[])
{

	PlayerClient robot("localhost");
	Position2dProxy p2dProxy(&robot,0);
	SonarProxy rangerProxy(&robot,0);

	//enable motors
	p2dProxy.SetMotorEnable(1);

	//variable decleration
	double forwardSpeed, turnSpeed;
	
	while(true)
	{
      //Request Geometries
      p2dProxy.RequestGeom();
      rangerProxy.RequestGeom();

		// read from the proxies
		robot.Read();

		//Move Straight
		//MoveStraight(&forwardSpeed, &turnSpeed);

      //Move to Objective - Shortest Path
      ShortestPath(p2dProxy, &forwardSpeed, &turnSpeed);

		//Avoid Obstacle
		ObstacleAvoid(rangerProxy, &forwardSpeed, &turnSpeed);

		//set motors
		p2dProxy.SetSpeed(forwardSpeed, turnSpeed);
	}
   
	return 0;
}
