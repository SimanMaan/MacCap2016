import RPi.GPIO as GPIO # always needed with RPi.GPIO  
import time, math

servo1 = 6
dir1 = 13
servo2 = 26
dir2 =19
encoder1a = 20 
encoder1b = 21
encoder2a = 16
encoder2b = 12

#robot variables
homeX = 0
homeY = 0
robotHeading = 0
robotPositionX = homeX
robotPositionY = homeY

GPIO.setwarnings(False)  
GPIO.setmode(GPIO.BCM)  # choose BCM pinout
GPIO.setup(dir1, GPIO.OUT, initial=1)#direction bit1
GPIO.setup(servo1, GPIO.OUT)#servo1
GPIO.setup(dir2, GPIO.OUT, initial=0)#direction bit2
GPIO.setup(servo2, GPIO.OUT)#servo2
leftMotor = GPIO.PWM(servo1, 50) #set up servo 1 at 50Hz
rightMotor = GPIO.PWM(servo2, 50) #set up servo 2 at 50Hz
GPIO.setup(encoder1a,GPIO.IN) #initialize encoder GPIOs
GPIO.setup(encoder1b,GPIO.IN)
GPIO.setup(encoder2a, GPIO.IN)
GPIO.setup(encoder2b, GPIO.IN)

countw1 = 0 #initialize encoder counts
countw2 = 0
COUNTSPERREV = 1200.0
WHEELDIA = 0.073 #meters
ticksToMeters = (WHEELDIA *  math.pi)/COUNTSPERREV #meters
metersToTicks = COUNTSPERREV/( math.pi * WHEELDIA) #ticks

def wheel1a(channel):
	if GPIO.input(encoder1a) == 1:
		if GPIO.input(encoder1b) == 0:
			count1(1,-1)
		else: count1(1,1)
	elif GPIO.input(encoder1a) == 0:
		if GPIO.input(encoder1b) == 0:
			count1(1,1)
		else: count1(1,-1) 

def wheel1b(channel):
	if GPIO.input(encoder1b) == 1:
		if GPIO.input(encoder1a) == 0:
			count1(1,1)
		else: count1(1,-1)
	elif GPIO.input(encoder1b) == 0:
		if GPIO.input(encoder1a) == 0:
			count1(1,-1)
		else: count1(1,1)
def wheel2a(channel):
	if GPIO.input(encoder2a) == 1:
		if GPIO.input(encoder2b) == 0:
			count1(2,-1)
		else: count1(2,1)
	elif GPIO.input(encoder2a) == 0:
		if GPIO.input(encoder2b) == 0:
			count1(2,1)
		else: count1(2,-1) 

def wheel2b(channel):
	if GPIO.input(encoder2b) == 1:
		if GPIO.input(encoder2a) == 0:
			count1(2,1)
		else: count1(2,-1)
	elif GPIO.input(encoder2b) == 0:
		if GPIO.input(encoder2a) == 0:
			count1(2,-1)
		else: count1(2,1)

GPIO.add_event_detect(encoder1a, GPIO.BOTH, callback = wheel1a)
GPIO.add_event_detect(encoder1b, GPIO.BOTH, callback = wheel1b)
GPIO.add_event_detect(encoder2a, GPIO.BOTH, callback = wheel2a)
GPIO.add_event_detect(encoder2b, GPIO.BOTH, callback = wheel2b)

def cleanup():
	leftMotor.stop()
	rightMotor.stop()
	GPIO.cleanup()
	
def count1(wheel,num):

	global countw1, countw2
	if wheel == 1:
		countw1 = countw1 + num
	elif wheel == 2:
		countw2 = countw2 + num
	#print countw1, countw2
def updatePos():
	global COUNTSPERREV,ticksToMeters,countw1, countw2
	displacement = (countw1 + countw2)*ticksToMeters/2
	return displacement
def setMotors(leftDir, rightDir, leftDuty, rightDuty): #leftDuty, rightDuty
	if leftDuty > 100:
		leftDuty = 100
	elif leftDuty < 0:
		leftDuty = 0
	if rightDuty > 100:
		rightDuty = 100
	elif rightDuty < 0:
		rightDuty = 0

	if leftDir == 1:
		GPIO.output(dir1,GPIO.HIGH)
	elif leftDir == -1: 
		GPIO.output(dir1,GPIO.LOW)
	if rightDir == 1:
		GPIO.output(dir2,GPIO.HIGH)
	elif rightDir == -1: 
		GPIO.output(dir2,GPIO.LOW)

	leftMotor.ChangeDutyCycle(leftDuty)
	rightMotor.ChangeDutyCycle(rightDuty)

def turnBot(rads,dir,duty):

	global countw1, countw2
	ticks = (rads/(2*3.14159) * 3750)
	targetw1 = countw1 + dir*ticks
	targetw2 = countw2 + -1*dir*ticks
	setMotors(dir,(-1*dir), duty,duty)
	while dir*countw1 < dir*targetw1 or -1*dir*countw2 < -1*dir*targetw2: # or = when both pass we stop
		time.sleep(.01)
		kp=float(1)/5.0
		e1 = (-1*dir*countw2)-dir*countw1
		e2 = dir*countw1-(-1*dir*countw2)
		Lduty = duty+e1*kp
		Rduty = duty+e2*kp
		#print "LEFT",dir*countw1, dir*targetw1, e1, "RIGHT",-1*dir*countw2, -1*dir*targetw2, e2
		#print countw1,countw2, Lduty,Rduty,e1
		if dir == 1:
		      print "turning " + str(rads) + " clockwise nigga"
		if dir == -1:
		      print "turning " + str(rads) + " counterclockwise nigga"
		setMotors(dir,(-1*dir), Lduty,Rduty)


def driveBot(dist,dir, duty):
	global countw1, countw2, robotPositionX, robotPositionY, currentHeading
	targetw1 = countw1 + dir * dist * metersToTicks
	targetw2 = countw2 + dir * dist * metersToTicks
	setMotors(dir,(dir), duty,duty)
	while dir*countw1 < dir*targetw1 or dir*countw2 < dir*targetw2: # or = when both pass we stop
		time.sleep(.01)
		kp=float(1)/5.0
		e1 = (dir*countw2)-(dir*countw1)
		e2 = (dir*countw1)-(dir*countw2)
		Lduty = duty+e1*kp
		Rduty = duty+e2*kp

		print "driving"
		#print "LEFT",dir*countw1,"RIGHT",-1*dir*countw2
		#print "Meters:",ticksToMeters*countw1, Lduty,Rduty,e1
		#print (countw1,countw2)
		setMotors(dir,(dir), Lduty,Rduty)

def calculateHeading(currentHeading, currentX, currentY, newX, newY):#current heading must always be positive and less than 2 pi
	xdifference = newX-currentX
	ydifference = newY-currentY
	angle = math.atan2(ydifference, xdifference)
	if angle < 0:
		angle = 2*math.pi +angle
	angle = angle - currentHeading
	if angle > math.pi:
		angle = angle - 2*math.pi 
	if angle < -1* math.pi:
		angle = angle + 2*math.pi
	return angle


def path(points): #assume that the starting robot heading is the positive x direction
	global robotHeading, robotPositionX, robotPositionY, counter
	angle = 0
	direction = 1
	distance = 0
	for i in range (0, len(points)/2):
		angle = calculateHeading(robotHeading, robotPositionX, robotPositionY, points[i][0], points[i][1])
		if angle < 0:
			direction = -1
		else: direction = 1 
		turnBot(abs(angle), direction, 50)
		robotHeading = robotHeading + angle
		distance = math.sqrt((robotPositionX - points[i][0])**2 + (robotPositionY - points[i][1])**2)
		driveBot(distance,1, 50)
		



def line (p1, p2):

    A = (p1[1] - p2[1])
    B = (p2[0] - p1[0])
    C = (p1[0] * p2[1] - p2[0] * p1[1])
    return A, B, -C

def intersection(L1, L2):
    D  = L1[0] * L2[1] - L1[1] * L2[0]
    Dx = L1[2] * L2[1] - L1[1] * L2[2]
    Dy = L1[0] * L2[2] - L1[2] * L2[0]
    if D != 0:
        x = Dx / D
        y = Dy / D
        return x, y
    else:
        return False

def inBounds(robotPos,boundaryLines):
        robotLines = line((robotPos[0],1000), (robotPos[0],-1000))
        robotLinesy = line((-1000, robotPos[1]), (1000, robotPos[1]))
        Bline = (0,0,0)
        count = 0
        Blines = []
        BlinesY = []
        intersections = []
        intersectionsY =[]
        for i in range (0, len(boundaryLines)//2):
                if(i + 1 == len(boundaryLines)//2):
                        if ((robotPos[0]<=boundaryLines[0] or robotPos[0]<=boundaryLines[i*2]) and (robotPos[0]>=boundaryLines[0] or robotPos[0]>=boundaryLines[i*2])):
                                Bline = line((boundaryLines[0],boundaryLines[1]),(boundaryLines[i*2],boundaryLines[i*2+1]))
                                Blines.append(Bline)

                        if ((robotPos[1]<=boundaryLines[1] or robotPos[1]<=boundaryLines[i*2+1]) and (robotPos[1]>=boundaryLines[1] or robotPos[1]>=boundaryLines[i*2+1])):
                                Bline = line((boundaryLines[0],boundaryLines[1]),(boundaryLines[i*2],boundaryLines[i*2+1]))
                                BlinesY.append(Bline)

                else:
                        if ((robotPos[0]<=boundaryLines[i*2] or robotPos[0]<=boundaryLines[(i*2) +2]) and (robotPos[0]>=boundaryLines[i*2] or robotPos[0]>=boundaryLines[i*2+2])):
                                Bline = line((boundaryLines[(i*2)],boundaryLines[(i*2)+1]),(boundaryLines[(i*2)+2],boundaryLines[(i*2)+3]))
                                Blines.append(Bline)

                        if ((robotPos[1]<=boundaryLines[i*2 + 1] or robotPos[1]<=boundaryLines[(i*2) +3]) and (robotPos[1]>=boundaryLines[i*2 + 1] or robotPos[1]>=boundaryLines[i*2+3])):
                                Bline = line((boundaryLines[(i*2)],boundaryLines[(i*2)+1]),(boundaryLines[(i*2)+2],boundaryLines[(i*2)+3]))
                                BlinesY.append(Bline)

        for j in range (0, len(Blines)):
                R = intersection(robotLines,Blines[j])
                if R:
                        count = count + 1
                        intersections.append(R)

        for i in range (0, len(BlinesY)):
                R = intersection(robotLinesy,BlinesY[i])
                if R:
                        intersectionsY.append(R)

        check = 0
        checkY = 0
        for i in range(0,len(intersections)//2):
                intersections.sort()
                if (intersections[i*2][1]<= robotPos[1] and intersections[i*2 + 1][1]>=robotPos[1]):
                        check = 1
                        break
        for i in range(0,len(intersectionsY)//2):
                intersectionsY.sort()
                if (intersectionsY[i*2][0]<=robotPos[0] and intersectionsY[i*2 + 1][0]>=robotPos[0]):
                        checkY = 1
                        break
        if (check == 1 or checkY == 1):
                return True
        else:
                return False

class Robot:

	def __init__(self, positionx, positiony, home):
		self.posx = positionx
		self.posy = positiony
		self.inbounds = True
		self.points = []
		self.allPoints = []
		self.circles = []
		self.count = 0
		self.home = home
		homex = home[0]
		homey = home[1]
		self.number = 0


	def updatePos(self,positionx, positiony):
		self.posx += positionx
		self.posy += positiony

	def inBounds(self, status):
		shortest = 10000000
		index = 0
		movex = 0
		movey = 0
		line = 0
		self.inbounds = status
		if self.number == self.count:
			print "[Course covered]"
		else:
			if self.inbounds == False:
				for i in range(0, len(self.allPoints)):
					distance = math.sqrt((self.posx - self.allPoints[i][0])**2 +  (self.posy - self.allPoints[i][1])**2)
					if distance < shortest:
						shortest = distance
						index = i
				movex = -(self.posx - self.allPoints[index][0])/shortest
				movey = -(self.posy - self.allPoints[index][1])/shortest
			else: 
				shortest = math.sqrt((self.posx - self.points[0][0])**2 +  (self.posy - self.points[0][1])**2)
				movex = -(self.posx - self.points[0][0])/shortest
				movey = -(self.posy - self.points[0][1])/shortest
				if shortest <= 1:
					self.points.pop(0)
					self.count = self.count + 1
		return line,movex,movey

	def getBounds (self):
		return self.inbounds

	def getPos(self):
		robotPos = [self.posx,self.posy]
		return robotPos
	def path(self, boundaryLines):
		reverse = True
		for i in range (0 ,200):
			reverse = not(reverse)
			if reverse:
				for j in range(0 , 200):
					pos = [i*1,j*1]
					if (inBounds(pos, boundaryLines)):
						self.points.append(pos)
						self.allPoints.append(pos)
			else:
				for j in range(200 , 0,-1):
					pos = [i*1,j*1]
					if (inBounds(pos, boundaryLines)):
						self.points.append(pos)
						self.allPoints.append(pos)
		self.points.append(self.home)
		self.number = len(self.points)
		print self.allPoints
		return self.allPoints

def main():
	robotPos = [0,0,] #this will be read in
	inputBounds = [0,0, 0,10, 10,10, 10,0] #these will be read in at the start of the program
	homepos = [0,0]
	robot = Robot(robotPos[0],robotPos[1],homepos)
	
	
	try:  
		points = robot.path(inputBounds)
		#print points
		counterMax= len(points)
		counter = 0
		#print updatePos()
		turnBot(3.14159,-1,30)
	    	#driveBot(5,1,40) 
	    	#print calculateHeading(0, 0,0, -1, 1)
	    	#cleanup()
	    	#path(points)
	except KeyboardInterrupt:
		cleanup()
rightMotor.start(0)          
leftMotor.start(0)          
main()

# leftMotor.stop()
# rightMotor.stop()

