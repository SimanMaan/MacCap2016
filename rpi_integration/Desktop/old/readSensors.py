import RPi.GPIO as GPIO # always needed with RPi.GPIO  
import time, math

servo1 = 6
dir1 = 13
servo2 = 26
dir2 =19
encoder1a = 20 
encoder1b = 21
encoder2a = 16
encoder2b = 12

#robot variables
homeX = 0
homeY = 0
robotHeading = 0
robotPositionX = homeX
robotPositionY = homeY


GPIO.setwarnings(False)  
GPIO.setmode(GPIO.BCM)  # choose BCM pinout
GPIO.setup(dir1, GPIO.OUT, initial=1)#direction bit1
GPIO.setup(servo1, GPIO.OUT)#servo1
GPIO.setup(dir2, GPIO.OUT, initial=0)#direction bit2
GPIO.setup(servo2, GPIO.OUT)#servo2
leftMotor = GPIO.PWM(servo1, 50) #set up servo 1 at 50Hz
rightMotor = GPIO.PWM(servo2, 50) #set up servo 2 at 50Hz
GPIO.setup(encoder1a,GPIO.IN) #initialize encoder GPIOs
GPIO.setup(encoder1b,GPIO.IN)
GPIO.setup(encoder2a, GPIO.IN)
GPIO.setup(encoder2b, GPIO.IN)

countw1 = 0 #initialize encoder counts
countw2 = 0
COUNTSPERREV = 1200.0
WHEELDIA = 0.073 #meters
ticksToMeters = (WHEELDIA *  math.pi)/COUNTSPERREV #meters
metersToTicks = COUNTSPERREV/( math.pi * WHEELDIA) #ticks

def wheel1a(channel):
	if GPIO.input(encoder1a) == 1:
		if GPIO.input(encoder1b) == 0:
			count1(1,-1)
		else: count1(1,1)
	elif GPIO.input(encoder1a) == 0:
		if GPIO.input(encoder1b) == 0:
			count1(1,1)
		else: count1(1,-1) 

def wheel1b(channel):
	if GPIO.input(encoder1b) == 1:
		if GPIO.input(encoder1a) == 0:
			count1(1,1)
		else: count1(1,-1)
	elif GPIO.input(encoder1b) == 0:
		if GPIO.input(encoder1a) == 0:
			count1(1,-1)
		else: count1(1,1)
def wheel2a(channel):
	if GPIO.input(encoder2a) == 1:
		if GPIO.input(encoder2b) == 0:
			count1(2,-1)
		else: count1(2,1)
	elif GPIO.input(encoder2a) == 0:
		if GPIO.input(encoder2b) == 0:
			count1(2,1)
		else: count1(2,-1) 

def wheel2b(channel):
	if GPIO.input(encoder2b) == 1:
		if GPIO.input(encoder2a) == 0:
			count1(2,1)
		else: count1(2,-1)
	elif GPIO.input(encoder2b) == 0:
		if GPIO.input(encoder2a) == 0:
			count1(2,-1)
		else: count1(2,1)

GPIO.add_event_detect(encoder1a, GPIO.BOTH, callback = wheel1a)
GPIO.add_event_detect(encoder1b, GPIO.BOTH, callback = wheel1b)
GPIO.add_event_detect(encoder2a, GPIO.BOTH, callback = wheel2a)
GPIO.add_event_detect(encoder2b, GPIO.BOTH, callback = wheel2b)

def cleanup():
	leftMotor.stop()
	rightMotor.stop()
	GPIO.cleanup()
	
def count1(wheel,num):
	global countw1, countw2
	if wheel == 1:
		countw1 = countw1 + num
	elif wheel == 2:
		countw2 = countw2 + num
	
	# print( countw1, countw2)

def updatePos():
	global COUNTSPERREV,ticksToMeters,countw1, countw2
	displacement = (countw1 + countw2)*ticksToMeters/2
	return displacement
def setMotors(leftDir, rightDir, leftDuty, rightDuty): #leftDuty, rightDuty
	if leftDuty > 20:
		leftDuty = 20
	elif leftDuty < 0:
		leftDuty = 0
	if rightDuty > 20:
		rightDuty = 20
	elif rightDuty < 0:
		rightDuty = 0

	if leftDir == 1:
		GPIO.output(dir1,GPIO.HIGH)
	elif leftDir == -1: 
		GPIO.output(dir1,GPIO.LOW)
	if rightDir == 1:
		GPIO.output(dir2,GPIO.HIGH)
	elif rightDir == -1: 
		GPIO.output(dir2,GPIO.LOW)

	leftMotor.ChangeDutyCycle(leftDuty)
	rightMotor.ChangeDutyCycle(rightDuty)

def turnBot(rads,dir,duty):

	global countw1, countw2
	ticks = (rads/(2*3.14159) * 3750)
	targetw1 = countw1 + dir*ticks
	targetw2 = countw2 + -1*dir*ticks
	print targetw1
	print targetw2
	

	setMotors(dir,(-1*dir), duty,duty)
	while -1*dir*countw1 < dir*targetw1 or dir*countw2 < -1*dir*targetw2: # or = when both pass we stop
		time.sleep(.01)
		kp=float(1)/5.0
		e1 = (-1*dir*countw2)-dir*countw1
		e2 = dir*countw1-(-1*dir*countw2)
		Lduty = duty+e1*kp
		Rduty = duty+e2*kp
		print "LEFT",dir*countw1, dir*targetw1, e1, "RIGHT",-1*dir*countw2, -1*dir*targetw2, e2
		#print countw1,countw2, Lduty,Rduty,e1
		#print "turning"
		setMotors(dir,(-1*dir), Lduty,Rduty)


def driveBot(dist,dir, duty):
	global countw1, countw2
	targetw1 = countw1 + dir * dist * metersToTicks
	targetw2 = countw2 + dir * dist * metersToTicks
	setMotors(dir,(dir), duty,duty)
	while dir*countw1 < dir*targetw1 or dir*countw2 < dir*targetw2: # or = when both pass we stop
		time.sleep(.01)
		kp=float(1)/5.0
		e1 = (dir*countw2)-(dir*countw1)
		e2 = (dir*countw1)-(dir*countw2)
		Lduty = duty+e1*kp
		Rduty = duty+e2*kp
		#print "LEFT",dir*countw1,"RIGHT",-1*dir*countw2
		#print "Meters:",ticksToMeters*countw1, Lduty,Rduty,e1
		print (countw1,countw2)
		setMotors(dir,(dir), Lduty,Rduty)

def calculateHeading(currentHeading, currentX, currentY, newX, newY):#current heading must always be positive and less than 2 pi
	xdifference = newX-currentX
	ydifference = newY-currentY
	angle = math.atan2(ydifference, xdifference)
	if angle < 0:
		angle = 2*math.pi +angle
	angle = angle - currentHeading
	if angle > math.pi:
		angle = angle - 2*math.pi 
	if angle < -1* math.pi:
		angle = angle + 2*math.pi
	return angle


def path(points): #assume that the starting robot heading is the positive x direction
	global robotHeading, robotPositionX, robotPositionY
	angle = 0
	direction = 1
	distance = 0
	for i in range (0, len(points)/2):
		angle = calculateHeading(robotHeading, robotPositionX, robotPositionY, points[i*2], points[i*2 + 1])
		if angle < 0:
			direction = -1
		else: direction = 1 
		turnBot(abs(angle), direction, 10)
		robotHeading = robotHeading + angle
		distance = math.sqrt((robotPositionX - points[i * 2])**2 + (robotPositionY - points[i*2+1])**2)
		driveBot(distance,1, 10)
		robotPositionX =  points[i * 2]
		robotPositionY = points[i * 2 + 1]

def main():
	
	try:  
		points = [1,0,1,1,0,1,0,0]
		#print updatePos()
		#turnBot(3.14159*2,-1,7)
	    	driveBot(.5,1,10) 
	    	
	    	#print calculateHeading(0, 0,0, -1, 1)
	    	#path(points)
	except KeyboardInterrupt:
		cleanup()




rightMotor.start(0)          
leftMotor.start(0)          
main()

# leftMotor.stop()
# rightMotor.stop()

