import time, math
import serial, time
from math import pi,cos,sin

#com_port  = '/dev/cu.usbmodem1411' ##change the COM port if required, check in Arduino IDE
com_port = '/dev/ttyAMC0'
baud_rate = 115200
offset = 12 #this is the degree offset of the lidar .. from tests we got 11.5 - 13 degree offset. Play with this value in the range to find best results


#/*Define  ants*/
 #define crucial sonar ranges
k_Sens_Max_Dist = 5000
k_Sens_Mid_Dist = 2000
k_Sens_Min_Dist = 1000
k_Sens_Stop_Dist = 500

turn180 = 180 #3.14159
turn90 = 90 #3.14159/2
turn45 = 45 #3.14159/4
turnSmall = 36  #3.14159/6
turnNone = 0

 # #define num of sensors
numSensFront = 4
numSensFrontL = 2
numSensFrontR = 2
numSensBack = 4
numSensBackL = 2
numSensBackR = 2

clear = True
previousTime = 0

##find minimum value in list and return
def minimum_value(x):
  ##set to maximum range to avoid edge case of first element being -1, because then if statement will not execute
  min_val = 99999

  for i in x[1:]:
    if i < min_val and i >= 0:
      #print i 
      min_val = i
  return min_val
 
def turnBot(rads,dir,duty):
  if dir == 0:
    print "turning %s in the clockwise direction" % (rads)
  else:
    print "turning %s in the counter clockwise direction" % (rads)

  #time.sleep(1)


#/*Obstacle avoidance algorithm*/
def collisionAvoid(quad1, quad2,quad3,quad4):

  global clear, previousTime

  #define the front sensors
  m_SensC_FrontRR = quad1
  m_SensC_FrontRC = quad2
  m_SensC_FrontLC = quad3
  m_SensC_FrontLL = quad4

  #printf("%f, %f, %f, %f\n", m_SensC_FrontRR, m_SensC_FrontRC, m_SensC_FrontLC, m_SensC_FrontLL);

  #define total distances per edge of robot
  frontTotL = m_SensC_FrontLL + m_SensC_FrontLC
  frontTotR = m_SensC_FrontRR + m_SensC_FrontRC
  frontTot = frontTotL + frontTotR

   
  tolerance = 0.5 #used when comparing left sensors vs right sensors

  #Only care if our sensors are sensing any objects within range
  #Otherwise leave speed and turn angle unchanged and allow robot to continue as is
  if (frontTot < numSensFront*k_Sens_Max_Dist and (previousTime <= time.time() - 5 or clear)):
    clear = True
    #want to make a sharp turn on the spot if any of the sensors are within critical distance
    #A tolerance is used when comparing left sensors vs right sensors
    #This is necessary for rare cases where all sensors are within critical distance, in this case robot will reverse
    if (m_SensC_FrontRR <= k_Sens_Stop_Dist or m_SensC_FrontRC <= k_Sens_Stop_Dist or m_SensC_FrontLL <= k_Sens_Stop_Dist or m_SensC_FrontLC <= k_Sens_Stop_Dist):
      if (frontTotL + tolerance <= frontTotR):
        if (m_SensC_FrontLL <= k_Sens_Stop_Dist and m_SensC_FrontLC <= k_Sens_Stop_Dist):
          turnBot(turn180, 1, 10) #right
        elif (m_SensC_FrontLL <= k_Sens_Stop_Dist):
          turnBot(turn45, 1, 10)
        elif (m_SensC_FrontLC <= k_Sens_Stop_Dist):
          turnBot(turn45, 1, 10)            
      elif (frontTotR + tolerance < frontTotL):
        if (m_SensC_FrontRR <= k_Sens_Stop_Dist and m_SensC_FrontRC <= k_Sens_Stop_Dist):
          turnBot(turn180, 0, 10)
        elif (m_SensC_FrontRR <= k_Sens_Stop_Dist):
          turnBot(turn45, 0, 10)
        elif (m_SensC_FrontRC <= k_Sens_Stop_Dist):
          turnBot(turn45, 0, 10)
    
     #None of the sensors are within critical distance, but within range of object
     #Must decrease speed and gradually adjust direction
    elif not (m_SensC_FrontRR <= k_Sens_Stop_Dist or m_SensC_FrontRC <= k_Sens_Stop_Dist or m_SensC_FrontLL <= k_Sens_Stop_Dist or m_SensC_FrontLC <= k_Sens_Stop_Dist):
      if (frontTotL <= frontTotR):
        if (m_SensC_FrontLL <= k_Sens_Min_Dist and m_SensC_FrontLC <= k_Sens_Min_Dist):
          turnBot(turn45, 1, 10)
        elif (m_SensC_FrontLL <= k_Sens_Min_Dist or m_SensC_FrontLC <= k_Sens_Min_Dist):
          turnBot(turnSmall, 1, 10)
      elif (frontTotL > frontTotR):
        if (m_SensC_FrontRR <= k_Sens_Min_Dist and m_SensC_FrontRC <= k_Sens_Min_Dist):
          turnBot(turn45, 0, 10)
        elif (m_SensC_FrontRR <= k_Sens_Min_Dist or m_SensC_FrontRC <= k_Sens_Min_Dist):
          turnBot(turnSmall, 0, 10)
      else: pass  #leave as set prior
    else: pass  #leave as set prior
  else: pass  #leave as set prior

def main():
  print "main"
  ser = serial.Serial(com_port,baud_rate)
  print "serial port opened"
  dist_mm_store = [-1]*361 #181
  global clear, previousTime

  while True:
    try:
      print "hello there"
      b = (ord(ser.read(1))) ##initial read
      print "yo asfdsadf"
      dati = []

      while True:
        ##250 == FA, FA is the start value - it's constant
        ##Each data packet is 22 bytes, > 20 means len(dati) == at least 21
        if b==(250) and len(dati)>20: 
          break

        ##add data to list, read again
        dati.append(b)
        b = (ord(ser.read(1)))

        ##do not hog the processor power - Python hogs 100% CPU without this in infinite loops
        time.sleep(0.00001) 
        
      if len(dati)==21:	
        ##index data packets go from 0xA0 (160) to 0xF9(259). Subtract 160 to normalize scale of data packets from 0 to 90.
        dati[0]=((dati[0])-160)  

        for i in (1,2,3,4):
          ##128 is an error code
          if dati[i*4] != 128:  
            ##if good data, convert value in dati to value in mm. code found online
            dist_mm = dati[4*i-1] | (( dati[4*i] & 0x3f) << 8) 
                                        
            ##dati[0] is index of each packet from 0 to 90. *4 for a value from 1 - 360, and cycle through the 4 data packets from that point at index
            ##e.g. dati[0] is 30. 30 * 4 = 120, then the values being read are 121, 122, 123, 124
            angle = dati[0]*4+i+1 

            ##adjust values by 2
            if angle < 361:#181:
              dist_mm_store[angle] = dist_mm
          else:
            #if good data, convert value in dati to value in mm. code found online
            dist_mm = -1 
                                        
            ##dati[0] is index of each packet from 0 to 90. *4 for a value from 1 - 360, and cycle through the 4 data packets from that point at index
            ##e.g. dati[0] is 30. 30 * 4 = 120, then the values being read are 121, 122, 123, 124
            angle = dati[0]*4+i+1 

            ##adjust values by 2
            if angle < 361:#181:
              dist_mm_store[angle] = dist_mm

        ##finding minimum distance in 180 degrees of vision, split into 45 degree quadrants    
        quad1 = minimum_value(dist_mm_store[45+offset:65+offset]) #currently not 45 degree, just avoiding the wheel... need to move lidar out more
        quad2 = minimum_value(dist_mm_store[0+offset:44+offset])
        
        quad3_1 = minimum_value(dist_mm_store[315+offset:359]) #should be to 315 to 360, but we want from 315+offset up to 0+offset so split to 2
        quad3_2 = minimum_value(dist_mm_store[0:0+offset])
        if quad3_1 < quad3_2:
          quad3 = quad3_1
        else:
          quad3 = quad3_2

        quad4 = minimum_value(dist_mm_store[290+offset:314+offset])  #currently not 45 degree, just avoiding the wheel... need to move lidar out more

        if (quad1 != 99999 and quad2 != 99999 and quad3 != 99999 and quad4 != 99999):
          print (quad1, quad2, quad3, quad4)
          if clear:
            previousTime = time.time()
            prevQuad1 = quad1
            prevQuad2 = quad2
            prevQuad3 = quad3
            prevQuad4 = quad4
            clear = False
          if (prevQuad1 < quad1 + 100 or prevQuad2 < quad2 + 100 or prevQuad3 < quad3 + 100 or prevQuad4 < quad4 + 100 and previousTime <= time.time() - 0.5):
            clear = True
          collisionAvoid(quad1,quad2,quad3,quad4)

    except KeyboardInterrupt:
      ser.close()
      print 'interrupted!'

main()
