import time, math
import serial, time, os, sys
from math import pi,cos,sin
import curses
import numpy as np
import logging

logging.basicConfig(filename="logfile.log",level=logging.DEBUG)

### display init
line1=""
line2=""
PRINT_DELAY = 0.1
last_print = time.time()
stdscr = curses.initscr()
curses.noecho()
curses.cbreak()
display_array_clean = np.array([[' ']*90]*21)
display_array = display_array_clean
front_view_2D_clean = np.zeros((2, 180))
front_view_2D = front_view_2D_clean
### serial init

com_port  = '/dev/ttyACM0'#'/dev/cu.usbmodem1411' ##change the COM port if required, check in Arduino IDE
baud_rate = 115200
ser = serial.Serial(com_port,baud_rate)

lidar_motor_speed = -1

#/*Define  ants*/
 #define crucial sonar ranges
# k_Sens_Max_Dist = 5000
# k_Sens_Mid_Dist = 2000
k_Sens_Min_Dist = 1000
k_Sens_Min_Ignore = 100
# k_Sens_Stop_Dist = 100

# turn180 = 180 #3.14159
# turn90 = 90 #3.14159/2
# turn45 = 45 #3.14159/4
# turn30 = 30  #3.14159/6
# turnNone = 0

 # #define num of sensors
# numSensFront = 4
# numSensFrontL = 2
# numSensFrontR = 2
# numSensBack = 4
# numSensBackL = 2
# numSensBackR = 2

clear = True
previousTime = 0


def update_front_view_array(dist_mm_store):
  global front_view_2D
  left = dist_mm_store[1:90]
  right = dist_mm_store[270:361]
  left.reverse()
  right.reverse()
  front_view = left+right
  #logging.debug("Length of front view array = %d",len(front_view))
  for i in range(len(front_view)):
    logging.debug("i=%d, front_view[i]=%d" % (i,front_view[i]))
    if front_view[i] == -1 and front_view_2D[0][i] != -1:
      front_view_2D[0][i]=-1
      front_view_2D[1][i]+=0
    elif front_view_2D[0][i] == -1:
      front_view_2D[0][i]=front_view[i]
      front_view_2D[1][i]+=1
    else:
      front_view_2D[0][i]+=front_view[i]
      front_view_2D[1][i]+=1



def update_display(dist_mm_store):
  global last_print,display_array,front_view_2D
  current_time = time.time()
  update_front_view_array(dist_mm_store)

  if (current_time - last_print) > PRINT_DELAY:
    fill_display_array()
    stdscr.addstr(0, 0, line1)
    stdscr.addstr(1, 0, "Motor: "+str(int(lidar_motor_speed))+"rpms")
    display_array[0][44],display_array[0][45]='(',')'
    for i in range(1,7):
      #y axis
      display_array[1000*i/300][0]=str(i)
      display_array[1000*i/300][89]=str(i)
      #x axis
      display_array[0][1000*i/133.4+44]=str(i)
      display_array[0][1000*(-1*i)/133.4+44]=str(i)
    for i in range(len(display_array)):
      line = '|'+str("".join(display_array[i]))+'|'
      stdscr.addstr(i+2, 0, line)
    #stdscr.addstr(2, 0, str(front_view_2D[0]))
    stdscr.refresh()
    display_array = display_array_clean
    front_view_2D = front_view_2D_clean
    last_print = current_time



def fill_display_array():
  global display_array,front_view_2D
  scaling = 90.0/len(front_view_2D[0])
  for i in range(len(front_view_2D[0])):
    if (front_view_2D[0][i] <=-1):
      display_array[0][int(i*scaling)] = ' '
      continue
    avg_val = front_view_2D[0][i]/front_view_2D[1][i]
    deg = i
    # x = int(int((avg_val*cos(pi/180*deg))+6000)/86)
    # y = int(avg_val*sin(pi/180*deg))/300
    x = (avg_val*cos(pi/180*deg)+6000)/133.4
    y = (avg_val*sin(pi/180*deg))/300
    #print x,y
    if (avg_val < k_Sens_Min_Ignore): #or (avg_val < 1000):
      continue
    elif (avg_val > 6000):
      logging.error("LIDAR: reading too large (%d,%d)" %(deg,avg_val))
    elif (avg_val < 6000):
      display_array[y][x] = 'o'

def fill_display_array_ascii():
  global display_array,front_view_2D
  scaling = 90.0/len(front_view_2D[0])
  for i in range(len(front_view_2D[0])):
    if (front_view_2D[0][i] <=-1):
      display_array[0][int(i*scaling)] = '?'
      continue
    avg_val = front_view_2D[0][i]/front_view_2D[1][i]
    if (avg_val < k_Sens_Min_Ignore):
      continue
    elif (avg_val < 500):
      display_array[0][int(i*scaling)] = '.'
    elif (avg_val < 1000):
      display_array[0][int(i*scaling)] = 'o'
    elif (avg_val < 1500):
      display_array[0][int(i*scaling)] = 'x'
    elif (avg_val < 3000):
      display_array[0][int(i*scaling)] = 'X'
    elif (avg_val < 6000):
      display_array[0][int(i*scaling)] = 'Z'
    else:
      display_array[0][int(i*scaling)] = ' '







##find minimum value in list and return
def minimum_value(x):
  ##set to maximum range to avoid edge case of first element being -1, because then if statement will not execute
  min_val = 99999
  for i in x[1:]:
    if i < min_val and i >= 0:
      min_val = i
  return min_val

def turnBot(rads,dir,duty):
  global line1
  if dir == 0:
    line1 =  "turning %s deg left" % (rads)
  else:
    line1 =  "turning %s deg right" % (rads)


  #time.sleep(1)


#/*Obstacle avoidance algorithm*/
def collisionAvoidErik(quads):
 # quad 1, quad2, quad3, quad 4
 # \\\\\\\\\ |||||||| /////////
 #         [robot_here]
  current_time = time.time()
  min_quad = minimum_value(quads)
  for i in range(4):
    if (quads[i] == min_quad) and (min_quad < k_Sens_Min_Dist):
        if i == 0:
            turnBot(45,1,10)
            break
        elif i == 1:
            turnBot(90,1,10)
            break
        elif i == 2:
            turnBot(90,0,10)
            break
        elif i == 3:
            turnBot(45,0,10)
            break
    elif (min_quad >= k_Sens_Min_Dist):
      turnBot(0,1,10)



def lidar_main(self):
  # init

  global clear, previousTime


  dist_mm_store = [-1]*361 #181



  while True:
    try:
      b = (ord(ser.read(1))) ##initial read
      dati = []


      while True:
        ##250 == FA, FA is the start value - it's constant
        ##Each data packet is 22 bytes, > 20 means len(dati) == at least 21
        if b==(250) and len(dati)>20:
          break

        ##add data to list, read again
        dati.append(b)
        b = (ord(ser.read(1)))

        ##do not hog the processor power - Python hogs 100% CPU without this in infinite loops
        time.sleep(0.00001)

      if len(dati)==21:
        ##index data packets go from 0xA0 (160) to 0xF9(259). Subtract 160 to normalize scale of data packets from 0 to 90.
        dati[0]=((dati[0])-160)

        for i in (1,2,3,4):
          ##128 is an error code
          if dati[i*4] != 128:
            ##if good data, convert value in dati to value in mm. code found online
            dist_mm = dati[4*i-1] | (( dati[4*i] & 0x3f) << 8)

            ##dati[0] is index of each packet from 0 to 90. *4 for a value from 1 - 360, and cycle through the 4 data packets from that point at index
            ##e.g. dati[0] is 30. 30 * 4 = 120, then the values being read are 121, 122, 123, 124
            angle = dati[0]*4+i+1

            ##adjust values by 2
            if angle < 361:#181:
              dist_mm_store[angle] = dist_mm
          else:
            #continue
            #if good data, convert value in dati to value in mm. code found online
            dist_mm = -1

            ##dati[0] is index of each packet from 0 to 90. *4 for a value from 1 - 360, and cycle through the 4 data packets from that point at index
            ##e.g. dati[0] is 30. 30 * 4 = 120, then the values being read are 121, 122, 123, 124
            angle = dati[0]*4+i+1

            ##adjust values by 2
            if angle < 361:#181:
              dist_mm_store[angle] = dist_mm

        ##finding minimum distance in 180 degrees of vision, split into 45 degree quadrants
        quad4 = minimum_value(dist_mm_store[45:60]) #currently not 45 degree, just avoiding the wheel... need to move lidar out more
        quad3 = minimum_value(dist_mm_store[0:44])
        quad2 = minimum_value(dist_mm_store[315:359])
        quad1 = minimum_value(dist_mm_store[295:314])  #currently not 45 degree, just avoiding the wheel... need to move lidar out more
        global lidar_motor_speed
        lidar_motor_speed = float( dati[1] | (dati[2] << 8) ) / 64.0
        update_display(dist_mm_store)


        if (quad1 != 99999 and quad2 != 99999 and quad3 != 99999 and quad4 != 99999):
          #print (quad1, quad2, quad3, quad4)
          if clear:
            previousTime = time.time()
            prevQuad1 = quad1
            prevQuad2 = quad2
            prevQuad3 = quad3
            prevQuad4 = quad4
            clear = False
          if (prevQuad1 < quad1 + 100 or prevQuad2 < quad2 + 100 or prevQuad3 < quad3 + 100 or prevQuad4 < quad4 + 100 and previousTime <= time.time() - 1):
            clear = True
          else:
            previousTime = time.time()
            prevQuad1 = quad1
            prevQuad2 = quad2
            prevQuad3 = quad3
            prevQuad4 = quad4
            clear = False
          collisionAvoidErik([quad1,quad2,quad3,quad4])


    except (KeyboardInterrupt, SystemExit):

      print 'keyboard inturrupt!'
      curses.echo()
      curses.nocbreak()
      curses.endwin()
      ser.close()
      #raise
    #finally:




if __name__ == '__main__':
    curses.wrapper(lidar_main)
