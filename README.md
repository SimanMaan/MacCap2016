If you are committing to this repository, please be aware of the following:

-The master branch is to be used ONLY for software code. Documentation for the product can be found under the Documentation branch. Please check and ensure that you have chosen the right branch before committing!

-Please use best practices when merging and pushing commits. Make sure you checkout first, and that the code you commit will not cause great instability or damage to the product.

-Please ensure that you fork the branch from master for any feature work that you do. The master branch should always be a stable version of the program, and should not have any major instability issues.

Refer to online GitHub/Gitlab documentation if you are having trouble committing to the server.